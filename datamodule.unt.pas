unit datamodule.unt;

interface

uses
  SysUtils, Classes, DB, ADODB;

type
  TbancoDados = class(TDataModule)
    adoConn: TADOConnection;
    adoQConfiguracao: TADOQuery;
    adoQFornecedores: TADOQuery;
    adoQFornecedorescodigo: TAutoIncField;
    adoQFornecedoresnome: TWideStringField;
    adoQFornecedoresrazaosocial: TWideStringField;
    adoQFornecedorescnpj: TWideStringField;
    adoQFornecedorescpf: TWideStringField;
    adoQFornecedoresend_logradouro: TWideStringField;
    adoQFornecedoresend_bairro: TWideStringField;
    adoQFornecedoresend_num: TIntegerField;
    adoQFornecedoresend_cep: TWideStringField;
    adoQFornecedoresend_cidade: TWideStringField;
    adoQFornecedoresend_uf: TWideStringField;
    adoQFornecedorestelefone: TWideStringField;
    adoQFornecedoresfax: TWideStringField;
    adoQFornecedorescelular: TWideStringField;
    adoQFornecedoresemail: TWideStringField;
    adoQProdutos: TADOQuery;
    adoQNotaFiscal: TADOQuery;
    adoQProdutoscodigo: TAutoIncField;
    adoQProdutoscdbarra: TWideStringField;
    adoQProdutosnome: TWideStringField;
    adoQProdutospreco: TBCDField;
    adoQProdutostipo_unid: TWideStringField;
    adoQProdutosdescricao: TMemoField;
    adoQProdutoscod_fornecedor: TIntegerField;
    adoQProdutosquant_estoque: TIntegerField;
    adoQConfiguracaologin: TWideStringField;
    adoQConfiguracaosenha: TWideStringField;
    adoQConfiguracaonome: TWideStringField;
    adoQConfiguracaorazaosocial: TWideStringField;
    adoQConfiguracaocnpj: TWideStringField;
    adoQConfiguracaocpf: TWideStringField;
    adoQConfiguracaoie: TWideStringField;
    adoQCompra: TADOQuery;
    adoQNotaFiscalcodigo: TAutoIncField;
    adoQNotaFiscalquant_produto: TIntegerField;
    adoQNotaFiscalvalor_total: TBCDField;
    adoQNotaFiscalvalor_pago: TBCDField;
    adoQNotaFiscalvalor_troco: TBCDField;
    adoQNotaFiscalstatus: TWideStringField;
    adoQCompracodigo: TAutoIncField;
    adoQCompranotafiscal: TIntegerField;
    adoQCompraitem_num: TIntegerField;
    adoQCompraitem_codigo: TWideStringField;
    adoQCompraitem_nome: TWideStringField;
    adoQCompraitem_valorunit: TBCDField;
    adoQCompraitem_quant: TBCDField;
    adoQCompraitem_valor: TBCDField;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  bancoDados: TbancoDados;

implementation

{$R *.dfm}

end.
