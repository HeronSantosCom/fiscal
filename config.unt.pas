unit config.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, DB, dxCore, dxButton, StdCtrls, Mask, DBCtrls, BmsXPLabel,
  ComCtrls, BmsXPPageControl, ExtCtrls;

type
  TfrmConfig = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    pclConfiguracao: TBmsXPPageControl;
    tstConfiguracao: TTabSheet;
    imgConfiguracao: TImage;
    BmsXPLabel1: TBmsXPLabel;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label12: TLabel;
    edtConfigLogin: TDBEdit;
    edtConfigSenha: TDBEdit;
    edtConfigNome: TDBEdit;
    edtConfigRazaoSocial: TDBEdit;
    edtConfigCnpj: TDBEdit;
    edtConfigCpf: TDBEdit;
    edtConfigIe: TDBEdit;
    btnConfigOk: TdxButton;
    btnConfigCancelar: TdxButton;
    dsConfiguracao: TDataSource;
    procedure btnConfigOkClick(Sender: TObject);
    procedure btnConfigCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtConfigCnpjKeyPress(Sender: TObject; var Key: Char);
    procedure edtConfigCpfKeyPress(Sender: TObject; var Key: Char);
    procedure edtConfigIeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfig: TfrmConfig;

implementation

uses datamodule.unt, imagem.unt;

{$R *.dfm}

procedure TfrmConfig.btnConfigOkClick(Sender: TObject);
begin
  bancoDados.adoQConfiguracao.Post;
  bancoDados.adoQConfiguracao.Close;
  Close;
end;

procedure TfrmConfig.btnConfigCancelarClick(Sender: TObject);
begin
  bancoDados.adoQConfiguracao.Cancel;
  bancoDados.adoQConfiguracao.Close;
  Close;
end;

procedure TfrmConfig.FormShow(Sender: TObject);
begin
  imgFundo.Picture := frmImagem.imgFundoGrande.Picture;
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  imgConfiguracao.Picture := frmImagem.imgConfiguracao.Picture;
  bancoDados.adoQConfiguracao.Open;
  bancoDados.adoQConfiguracao.Edit;
  edtConfigLogin.SelectAll;
end;

procedure TfrmConfig.edtConfigCnpjKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmConfig.edtConfigCpfKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmConfig.edtConfigIeKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

end.
