unit login.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BmsXPLabel, StdCtrls, TFlatEditUnit, dxCore, dxButton, pngimage,
  ExtCtrls;

type
  TfrmLogin = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    btnSair: TdxButton;
    btnAutenticar: TdxButton;
    edtUsuario: TFlatEdit;
    edtSenha: TFlatEdit;
    lblUsuario: TBmsXPLabel;
    lblSenha: TBmsXPLabel;
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnAutenticarClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmLogin: TfrmLogin;

implementation

uses desktop.unt, imagem.unt, datamodule.unt;

{$R *.dfm}

procedure TfrmLogin.btnSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmLogin.FormShow(Sender: TObject);
begin
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  imgFundo.Picture := frmImagem.imgFundoPequeno.Picture;
end;

procedure TfrmLogin.btnAutenticarClick(Sender: TObject);
var
  userLogin, userSenha, flagErro:String;
begin
//  bancoDados.adoQConfiguracao.Active := True;
  userLogin := edtUsuario.Text;
  userSenha := edtSenha.Text;
  if (userLogin = '') or (userSenha = '') then
  begin
    ShowMessage('Usu�rio ou Senha n�o especificado!');
  end else
  begin
    if bancoDados.adoQConfiguracao.FindField('login').Value <> userLogin then
      ShowMessage('Usu�rio Incorreto!')
    else if bancoDados.adoQConfiguracao.FindField('senha').Value <> userSenha then
      ShowMessage('Senha Incorreta!')
    else
    begin
      //bancoDados.adoQConfiguracao.Active := False;
      frmDesktop.Show;
      frmDesktop.userLogin := userLogin;
      Free;
    end;
  end;
end;

end.
