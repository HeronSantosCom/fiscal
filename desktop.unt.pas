unit desktop.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls, dxCore, dxButton, XP_Button, BmsXPLabel,
  StdCtrls, DB, ADODB;

type
  TfrmDesktop = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    imgMarcadAgua: TImage;
    btnSair: TdxButton;
    btnConfiguracao: TtfXPButton;
    btnCaixa: TtfXPButton;
    lblConfiguracao: TBmsXPLabel;
    lblCaixa: TBmsXPLabel;
    btnSobre: TtfXPButton;
    lblSobre: TBmsXPLabel;
    btnProdutos: TtfXPButton;
    lblProdutos: TLabel;
    btnFornecedores: TtfXPButton;
    lblFornecedores: TLabel;
    procedure btnSairClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure btnSobreClick(Sender: TObject);
    procedure btnFornecedoresClick(Sender: TObject);
    procedure btnProdutosClick(Sender: TObject);
    procedure btnCaixaClick(Sender: TObject);
    procedure btnConfiguracaoClick(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    userLogin: String;
  end;

var
  frmDesktop: TfrmDesktop;

implementation

uses imagem.unt, sobre.unt, sysUpdate.unt, gerente.unt, datamodule.unt,
  configuracao.unt, config.unt;

{$R *.dfm}

procedure TfrmDesktop.btnSairClick(Sender: TObject);
begin
  Application.Terminate;
end;

procedure TfrmDesktop.FormShow(Sender: TObject);
begin
  imgFundo.Picture := frmImagem.imgFundoGrande.Picture;
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  imgMarcadAgua.Picture := frmImagem.imgMarcaDagua.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  btnCaixa.Glyph := frmImagem.imgCaixa.Picture;
  btnConfiguracao.Glyph := frmImagem.imgConfiguracao.Picture;
  btnSobre.Glyph := frmImagem.imgSobre.Picture;
  btnProdutos.Glyph := frmImagem.imgProdutos.Picture;
  btnFornecedores.Glyph := frmImagem.imgFornecedores.Picture;
end;

procedure TfrmDesktop.btnSobreClick(Sender: TObject);
begin
  frmSobre.Show;
end;

procedure TfrmDesktop.btnFornecedoresClick(Sender: TObject);
begin
  frmGerente.Show;
  frmGerente.pclGerente.ActivePageIndex := 1;
end;

procedure TfrmDesktop.btnProdutosClick(Sender: TObject);
begin
  frmGerente.Show;
  frmGerente.pclGerente.ActivePageIndex := 2;
end;

procedure TfrmDesktop.btnCaixaClick(Sender: TObject);
begin
  frmGerente.Show;
  frmGerente.pclGerente.ActivePageIndex := 0;
end;

procedure TfrmDesktop.btnConfiguracaoClick(Sender: TObject);
begin
  frmConfig.Show;
  frmConfig.pclConfiguracao.ActivePageIndex := 0;
end;

end.
