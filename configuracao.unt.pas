unit configuracao.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BmsXPLabel, XP_Button, ExtCtrls, DB, dxCore, dxButton, StdCtrls,
  Mask, DBCtrls, ComCtrls, BmsXPPageControl;

type
  TfrmConfiguracao = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    pclConfiguracao: TBmsXPPageControl;
    tstConfiguracao: TTabSheet;
    imgConfiguracao: TImage;
    BmsXPLabel1: TBmsXPLabel;
    Label1: TLabel;
    edtConfigLogin: TDBEdit;
    Label2: TLabel;
    edtConfigSenha: TDBEdit;
    Label3: TLabel;
    edtConfigNome: TDBEdit;
    Label4: TLabel;
    edtConfigRazaoSocial: TDBEdit;
    Label5: TLabel;
    edtConfigCnpj: TDBEdit;
    edtConfigCpf: TDBEdit;
    Label6: TLabel;
    edtConfigIe: TDBEdit;
    Label7: TLabel;
    Label12: TLabel;
    btnConfigOk: TdxButton;
    btnConfigCancelar: TdxButton;
    dsConfiguracao: TDataSource;
    procedure btnFecharClick(Sender: TObject);
    procedure btnConfigOkClick(Sender: TObject);
    procedure btnConfigCancelarClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtConfigCnpjKeyPress(Sender: TObject; var Key: Char);
    procedure edtConfigCpfKeyPress(Sender: TObject; var Key: Char);
    procedure edtConfigIeKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmConfiguracao: TfrmConfiguracao;

implementation

uses datamodule.unt, imagem.unt;

{$R *.dfm}

procedure TfrmConfiguracao.btnFecharClick(Sender: TObject);
begin
  Close;
end;

procedure TfrmConfiguracao.btnConfigOkClick(Sender: TObject);
begin
  bancoDados.adoQConfiguracao.Post;
  bancoDados.adoQConfiguracao.Close;
  Close;
end;

procedure TfrmConfiguracao.btnConfigCancelarClick(Sender: TObject);
begin
  bancoDados.adoQConfiguracao.Cancel;
  bancoDados.adoQConfiguracao.Close;
  Close;
end;

procedure TfrmConfiguracao.FormShow(Sender: TObject);
begin
  imgFundo.Picture := frmImagem.imgFundoGrande.Picture;
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  imgConfiguracao.Picture := frmImagem.imgConfiguracao.Picture;
  bancoDados.adoQConfiguracao.Open;
  bancoDados.adoQConfiguracao.Edit;
  edtConfigLogin.SelectAll;
end;

procedure TfrmConfiguracao.edtConfigCnpjKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmConfiguracao.edtConfigCpfKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmConfiguracao.edtConfigIeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

end.
