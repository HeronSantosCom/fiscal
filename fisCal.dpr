program fisCal;

uses
  Forms,
  SysUtils,
  Windows,
  tbSys in 'tbSys.pas',
  desktop.unt in 'desktop.unt.pas' {frmDesktop},
  splash.unt in 'splash.unt.pas' {frmSplash},
  login.unt in 'login.unt.pas' {frmLogin},
  imagem.unt in 'imagem.unt.pas' {frmImagem},
  sobre.unt in 'sobre.unt.pas' {frmSobre},
  gerente.unt in 'gerente.unt.pas' {frmGerente},
  notafiscal.unt in 'notafiscal.unt.pas' {frmNotaFiscal},
  datamodule.unt in 'datamodule.unt.pas' {bancoDados: TDataModule},
  config.unt in 'config.unt.pas' {frmConfig};

{$R *.res}
var
  i:ShortInt;
  maximo:ShortInt=0;
  divisor:ShortInt=8;

begin
  Application.Initialize;
  Application.Title := 'fisCal';

  with TfrmSplash.Create(nil) do
    try
      fgeProgresso.MaxValue:=100;
      Show;
      Update;

      lblStatus.Caption := 'Carregando m�dulo de seguranca...';
      lblStatus.Repaint;
      Application.CreateForm(TfrmLogin, frmLogin);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Carregando banco de dados...';
      lblStatus.Repaint;
      Application.CreateForm(TbancoDados, bancoDados);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Carregando m�dulo de configurac�o...';
      lblStatus.Repaint;
      Application.CreateForm(TfrmConfig, frmConfig);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Carregando imagens...';
      lblStatus.Repaint;
      Application.CreateForm(TfrmImagem, frmImagem);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Carregando desktop...';
      lblStatus.Repaint;
      Application.CreateForm(TfrmDesktop, frmDesktop);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Carregando...';
      lblStatus.Repaint;
      Application.CreateForm(TfrmGerente, frmGerente);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;

      Application.CreateForm(TfrmNotaFiscal, frmNotaFiscal);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      Application.CreateForm(TfrmSobre, frmSobre);
      for i:=1 to (maximo+(100 div divisor)) do
      begin
        fgeProgresso.Progress := i;
        maximo := i;
      end;
      Sleep(250);

      lblStatus.Caption := 'Iniciando...';
      lblStatus.Repaint;
      Sleep(1000);
    finally

    Free;
    Application.Run;
  end;
end.
