unit splash.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, TFlatPanelUnit, TFlatGaugeUnit, TFlatGroupBoxUnit,
  pngimage, BmsXPLabel;

type
  TfrmSplash = class(TForm)
    fgeProgresso: TFlatGauge;
    fplFundo: TFlatPanel;
    imgLogotipo: TImage;
    imgSimbolo: TImage;
    imgFundo: TImage;
    lblStatus: TBmsXPLabel;
    lblCarregando: TBmsXPLabel;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSplash: TfrmSplash;

implementation

uses imagem.unt;

{$R *.dfm}


end.
