unit sobre.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, BmsXPLabel, pngimage, ExtCtrls, tbSys, dxCore, dxButton,
  TFlatPanelUnit, StdCtrls;

type
  TfrmSobre = class(TForm)
    imgFundo: TImage;
    imgLogotipo: TImage;
    imgSimbolo: TImage;
    lblVerMono: TBmsXPLabel;
    fplFundo: TFlatPanel;
    lblVersao: TBmsXPLabel;
    lblBuild: TBmsXPLabel;
    lblLogado: TBmsXPLabel;
    lblWinVer: TBmsXPLabel;
    lblMemoria: TBmsXPLabel;
    btnFechar: TdxButton;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    procedure btnFecharClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmSobre: TfrmSobre;
  MemoryStatus: TMemoryStatus;

implementation

uses imagem.unt;

{$R *.dfm}

function GetBuildInfo:string;
var
  VerInfoSize: DWORD;
  VerInfo: Pointer;
  VerValueSize: DWORD;
  VerValue: PVSFixedFileInfo;
  Dummy: DWORD;
  V1, V2, V3, V4: Word;
  Prog : string;
begin
  Prog := Application.Exename;
  VerInfoSize := GetFileVersionInfoSize(PChar(prog), Dummy);
  GetMem(VerInfo, VerInfoSize);
  GetFileVersionInfo(PChar(prog), 0, VerInfoSize, VerInfo);
  VerQueryValue(VerInfo, '\', Pointer(VerValue), VerValueSize);
  with VerValue^ do
  begin
    V1 := dwFileVersionMS shr 16;
    V2 := dwFileVersionMS and $FFFF;
    V3 := dwFileVersionLS shr 16;
    V4 := dwFileVersionLS and $FFFF;
  end;
  FreeMem(VerInfo, VerInfoSize);
  result := Copy (IntToStr (100 + v1), 3, 2) + '.' +
  Copy (IntToStr (100 + v2), 3, 2) + '.' +
  Copy (IntToStr (100 + v3), 3, 2) + '.' + 
  Copy (IntToStr (100 + v4), 3, 2);
end;

procedure TfrmSobre.btnFecharClick(Sender: TObject);
begin
  Hide;
end;

procedure TfrmSobre.FormShow(Sender: TObject);
begin
  MemoryStatus.dwLength:= sizeof(MemoryStatus);
  GlobalMemoryStatus(MemoryStatus);
  imgFundo.Picture := frmImagem.imgFundoPequeno.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  lblVersao.Caption := 'Vers�o: 1.0';
  lblBuild.Caption := 'Build: ' + GetBuildInfo;
  lblMemoria.Caption := 'Total de mem�ria f�sica: ' + Format('%.2f',[MemoryStatus.dwTotalPhys/1048576]) + 'KB';
  lblLogado.Caption := 'Logado em: ' + SysUserName;
  lblWinVer.Caption := 'Vers�o do sistema: ' + SysVersionStr;
end;

end.
