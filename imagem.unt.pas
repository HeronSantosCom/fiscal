unit imagem.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, pngimage, ExtCtrls;

type
  TfrmImagem = class(TForm)
    imgMarcaDagua: TImage;
    imgFundoGrande: TImage;
    imgFundoPequeno: TImage;
    imgLogotipo: TImage;
    imgSimbolo: TImage;
    imgCaixa: TImage;
    imgConfiguracao: TImage;
    imgAtualizar: TImage;
    imgAjuda: TImage;
    imgSobre: TImage;
    imgIndex: TImage;
    imgClientes: TImage;
    imgProdutos: TImage;
    imgFechar: TImage;
    imgFornecedores: TImage;
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmImagem: TfrmImagem;

implementation

{$R *.dfm}

end.
