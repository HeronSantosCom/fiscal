unit gerente.unt;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, ExtCtrls, BmsXPLabel, TFlatTabControlUnit, ComCtrls,
  BmsXPPageControl, XP_Button, StdCtrls, Mask, TFlatMaskEditUnit,
  TFlatEditUnit, dxCore, dxButton, DB, ADODB, DBCtrls, Grids, DBGrids,
  CJVDBBarcode, SSBaseXP, SSEdit, TFlatPanelUnit;

type
  TfrmGerente = class(TForm)
    imgFundo: TImage;
    imgSimbolo: TImage;
    imgLogotipo: TImage;
    pclGerente: TBmsXPPageControl;
    tstCaixa: TTabSheet;
    tstProdutos: TTabSheet;
    tstFornecedores: TTabSheet;
    btnFechar: TtfXPButton;
    lblFechar: TBmsXPLabel;
    imgCaixa: TImage;
    imgFornecedores: TImage;
    imgProdutos: TImage;
    BmsXPLabel2: TBmsXPLabel;
    BmsXPLabel3: TBmsXPLabel;
    BmsXPLabel5: TBmsXPLabel;
    dsFornecedores: TDataSource;
    Label13: TLabel;
    edtFornecedoresNome: TDBEdit;
    Label14: TLabel;
    edtFornecedoresRazaoSocial: TDBEdit;
    Label15: TLabel;
    edtFornecedoresCnpj: TDBEdit;
    Label16: TLabel;
    edtFornecedorescpf: TDBEdit;
    Label17: TLabel;
    edtFornecedoresLogradouro: TDBEdit;
    Label18: TLabel;
    edtFornecedoresBairro: TDBEdit;
    Label19: TLabel;
    edtFornecedoresNum: TDBEdit;
    Label20: TLabel;
    edtFornecedoresCEP: TDBEdit;
    Label21: TLabel;
    edtFornecedoresCidade: TDBEdit;
    Label22: TLabel;
    edtFornecedoresUf: TDBEdit;
    Label23: TLabel;
    edtFornecedoresTel: TDBEdit;
    Label24: TLabel;
    edtFornecedoresFax: TDBEdit;
    Label25: TLabel;
    edtFornecedoresCel: TDBEdit;
    Label26: TLabel;
    edtFornecedoresEmail: TDBEdit;
    Label27: TLabel;
    Label43: TLabel;
    edtProdutosCodBarras: TDBEdit;
    dsProdutos: TDataSource;
    Label44: TLabel;
    edtProdutosNome: TDBEdit;
    Label48: TLabel;
    edtProdutosDescricao: TDBMemo;
    Label49: TLabel;
    Label50: TLabel;
    edtProdutosTipo: TDBComboBox;
    Label46: TLabel;
    edtProdutosEstoque: TDBEdit;
    btnProdutosBuscaCodBarras: TdxButton;
    btnProdutosBuscaNome: TdxButton;
    Label51: TLabel;
    btnProdutosCadastrar: TdxButton;
    btnProdutosModificar: TdxButton;
    btnProdutosRemover: TdxButton;
    btnProdutosCancelar: TdxButton;
    cbxProdutosFornecedoresHide: TComboBox;
    cbxProdutosFornecedores: TComboBox;
    Label45: TLabel;
    CJVDBBarcode1: TCJVDBBarcode;
    edtProdutosPreco: TDBEdit;
    FlatPanel1: TFlatPanel;
    FlatPanel2: TFlatPanel;
    FlatPanel3: TFlatPanel;
    FlatPanel4: TFlatPanel;
    lblNomeProduto: TLabel;
    lblValorUnit: TLabel;
    lblvalorTotal: TLabel;
    edtCaixaImpressao: TMemo;
    Label52: TLabel;
    edtCaixaCodBarra: TFlatEdit;
    btnFornecedoresBuscaCnpj: TdxButton;
    btnFornecedoresBuscaCpf: TdxButton;
    Label40: TLabel;
    btnFornecedoresCadastrar: TdxButton;
    btnFornecedoresModificar: TdxButton;
    btnFornecedoresRemover: TdxButton;
    btnFornecedoresCancelar: TdxButton;
    lblValor: TLabel;
    Label2: TLabel;
    BmsXPLabel1: TBmsXPLabel;
    BmsXPLabel4: TBmsXPLabel;
    BmsXPLabel6: TBmsXPLabel;
    procedure btnFecharClick(Sender: TObject);
    procedure edtFornecedorescpfKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresCnpjKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresTelKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresFaxKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresCelKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresCEPKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresNumKeyPress(Sender: TObject; var Key: Char);
    procedure btnFornecedoresCadastrarClick(Sender: TObject);
    procedure btnFornecedoresCancelarClick(Sender: TObject);
    procedure btnFornecedoresModificarClick(Sender: TObject);
    procedure btnFornecedoresRemoverClick(Sender: TObject);
    procedure cbxProdutosFornecedoresChange(Sender: TObject);
    procedure btnProdutosCadastrarClick(Sender: TObject);
    procedure btnProdutosModificarClick(Sender: TObject);
    procedure btnProdutosCancelarClick(Sender: TObject);
    procedure btnProdutosRemoverClick(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure edtCaixaCodBarraKeyPress(Sender: TObject; var Key: Char);
    procedure btnFornecedoresBuscaCnpjClick(Sender: TObject);
    procedure btnFornecedoresBuscaCpfClick(Sender: TObject);
    procedure btnProdutosBuscaCodBarrasClick(Sender: TObject);
    procedure btnProdutosBuscaNomeClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure edtClientesNumKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutosCodBarrasKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutosEstoqueKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutosNomeKeyPress(Sender: TObject; var Key: Char);
    procedure edtProdutosDescricaoKeyPress(Sender: TObject; var Key: Char);
    procedure edtClientesNomeKeyPress(Sender: TObject; var Key: Char);
    procedure edtClientesLogradouroKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtClientesBairroKeyPress(Sender: TObject; var Key: Char);
    procedure edtClientesCidadeKeyPress(Sender: TObject; var Key: Char);
    procedure edtClientesUfKeyPress(Sender: TObject; var Key: Char);
    procedure edtClientesDependentesKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedoresNomeKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresRazaoSocialKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedoresLogradouroKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedoresBairroKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedoresCidadeKeyPress(Sender: TObject;
      var Key: Char);
    procedure edtFornecedoresUfKeyPress(Sender: TObject; var Key: Char);
    procedure edtFornecedoresEmailKeyPress(Sender: TObject; var Key: Char);
  private
    { Private declarations }
  public
    itemNum, itemQTDTotal, notaFiscal: Integer;
    itemQTD, valorTotal: Real;
    { Public declarations }
  end;

var
  frmGerente: TfrmGerente;

implementation

uses imagem.unt, datamodule.unt, login.unt, desktop.unt, notafiscal.unt;

{$R *.dfm}

procedure TfrmGerente.FormShow(Sender: TObject);
//=================================================================
// > AO MOSTRAR O FORM. FA�A
//=================================================================
begin
  //CARREGA AS IMAGENS NO FORM. 'FRMIMAGEM'
  imgFundo.Picture := frmImagem.imgFundoGrande.Picture;
  imgLogotipo.Picture := frmImagem.imgLogotipo.Picture;
  imgSimbolo.Picture := frmImagem.imgSimbolo.Picture;
  btnFechar.Glyph := frmImagem.imgFechar.Picture;
  imgCaixa.Picture := frmImagem.imgCaixa.Picture;
  imgFornecedores.Picture := frmImagem.imgFornecedores.Picture;
  imgProdutos.Picture := frmImagem.imgProdutos.Picture;
end;

procedure TfrmGerente.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
//=================================================================
// > AO PRESSIONAR UMA TECLA FA�A
//=================================================================
var
  strCNPJ, strRazao, strData, strIE, strHora, valorRecebidoTmp: String;
  valorRecebido, valorTroco: Real;
begin
  // SE O TABCONTROL FOR 'CAIXA' FA�A
  If frmGerente.pclGerente.ActivePageIndex = 0 then
    // SE A TECLA PRESSIONADA FOR F1
    If Key = vk_F1 then
    begin
      // LIMPA OS CAMPOS
      edtCaixaImpressao.Clear;
      edtCaixaCodBarra.Enabled := True;
      bancoDados.adoQConfiguracao.Open; // ABRE A QUERY CONFIGURACAO
        // RESGATA OS CAMPOS DA QUERY
        strRazao := bancoDados.adoQConfiguracao.FieldByName('razaosocial').Value;
        strCNPJ := bancoDados.adoQConfiguracao.FieldByName('CNPJ').Value;
        strIE := bancoDados.adoQConfiguracao.FieldByName('ie').Value;
      bancoDados.adoQConfiguracao.Close; // FECHA A QUERY
      // RESGATA A DATA E A HORA
      strData := DateToStr(Date);
      strHora := TimeToStr(Time);

      // ZERA AS VARIAVEIS NUM.
      itemNum := 0;
      itemQTDTotal := 0;
      itemQTD := 0;
      valorTotal := 0;
      valorRecebido := 0;

      // INICIA A MEMO ADICIONANDO OS DADOS DA MERCEARIA
      edtCaixaImpressao.Lines.Add(strRazao);
      If length(strCNPJ) <> 0 Then
      begin
        if length(strIE) <> 0 Then
          edtCaixaImpressao.Lines.Add('CNPJ: ' + strCNPJ + ' - INSCRICAO ESTADUAL: ' + strIE)
        else
          edtCaixaImpressao.Lines.Add('CNPJ: ' + strCNPJ + ' - INSCRICAO ESTADUAL: ' + strIE);
      end;
      edtCaixaImpressao.Lines.Add(strData + ' - ' + strHora);
      edtCaixaImpressao.Lines.Add('---------------------------------------------------------------------------------');
      edtCaixaImpressao.Lines.Add('                                 CUPOM FISCAL');
      edtCaixaImpressao.Lines.Add('---------------------------------------------------------------------------------');
      edtCaixaCodBarra.SetFocus;
      // ABRE A QUERY NOTA FISCAL
      bancoDados.adoQNotaFiscal.Open;
      // ABRE PARA MODO DE INSER��O
      bancoDados.adoQNotaFiscal.Insert;
        // INDICA O STATUS DA NOTA FISCAL ATUAL PARA ABERTO
        bancoDados.adoQNotaFiscal.FieldByName('status').Value := 'ABERTO';
      // CONFIRMA A INSER��O
      bancoDados.adoQNotaFiscal.Post;
      // PEGA O CODIGO DA NOTA FISCAL EM ABERTO
      notaFiscal := bancoDados.adoQNotaFiscal.FieldByName('codigo').Value;
      // FECHA A QUERY
      bancoDados.adoQNotaFiscal.Close;
    end;
    // SE A TECLA FOR F11
    If Key = vk_F11 then
    begin
      // FECHA O CAIXA
      edtCaixaImpressao.Lines.Add('---------------------------------------------------------------------------------');
      edtCaixaImpressao.Lines.Add('  TOTAIS DE ITENS: ' + FloatToStr(itemNum));
      edtCaixaImpressao.Lines.Add('  TOTAL: ' + FloatToStrF(valorTotal,ffCurrency,18,2));

      valorRecebidoTmp := FloatToStr(valorTotal);

      // PERGUNTA QUANTO EST� RECEBENDO
      If InputQuery('PAGAMENTO A VISTA','RECEBENDO R$:',valorRecebidoTmp) = True Then
      begin
        valorRecebido :=  StrToFloat(valorRecebidoTmp);
      end;
      edtCaixaImpressao.Lines.Add('  DINHEIRO: ' + FloatToStrF(valorRecebido,ffCurrency,18,2));
      edtCaixaImpressao.Lines.Add('  VALOR RECEBIDO: ' + FloatToStrF(valorRecebido,ffCurrency,18,2));
      // CALCULA O TROCO
      valorTroco := valorRecebido - valorTotal;
      edtCaixaImpressao.Lines.Add('  TROCO: ' + FloatToStrF(valorTroco,ffCurrency,18,2));
      edtCaixaImpressao.Lines.Add('---------------------------------------------------------------------------------');

      // FECHA A NOTA FISCAL
      bancoDados.adoQNotaFiscal.Open;
      bancoDados.adoQNotaFiscal.Locate('codigo',notaFiscal,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQNotaFiscal.Edit;
        bancoDados.adoQNotaFiscal.FieldByName('quant_produto').Value := itemQTDTotal;
        bancoDados.adoQNotaFiscal.FieldByName('valor_total').Value := valorTotal;
        bancoDados.adoQNotaFiscal.FieldByName('valor_pago').Value := valorRecebido;
        bancoDados.adoQNotaFiscal.FieldByName('valor_troco').Value := valorTroco;
        bancoDados.adoQNotaFiscal.FieldByName('status').Value := 'FINALIZADO';
      bancoDados.adoQNotaFiscal.Post;
      bancoDados.adoQNotaFiscal.Close;

      // LIMPA OS CAMPOS
      lblNomeProduto.Caption := '';
      lblValorUnit.Caption := 'R$ 0,00';
      lblValor.Caption := 'R$ 0,00';
      lblvalorTotal.Caption := 'R$ 0,00';
      itemQTD := 0;
      edtCaixaCodBarra.Clear;
      edtCaixaCodBarra.Enabled := False;

      // CRIA A LISTA DE COMPRAS PARA IMPRESS�O DA NOTA FISCAL ATUAL
      bancoDados.adoQConfiguracao.Open;
      bancoDados.adoQNotaFiscal.Open;
      bancoDados.adoQNotaFiscal.Locate('codigo',notaFiscal,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQCompra.SQL.Clear;
      bancoDados.adoQCompra.SQL.Add('SELECT * FROM compra');
      bancoDados.adoQCompra.SQL.Add('WHERE notafiscal=' + IntToStr(notaFiscal));
      bancoDados.adoQCompra.Open;
      
      strData := DateToStr(Date);
      strHora := TimeToStr(Time);

      frmNotaFiscal.lblData.Caption := strData + ' - ' + strHora;
      // ABRE O RELAT�RIO DA NOTA FISCAL
      frmNotaFiscal.qrNotaFiscal.Preview;
    end;
end;

procedure TfrmGerente.edtCaixaCodBarraKeyPress(Sender: TObject;
  var Key: Char);
//=================================================================
// > AO DIGITAR NO CAMPO 'CODIGO BARRA'
//=================================================================
var
  itemCodigo, itemNome, itemTipo, itemQuantTemp: String;
  itemValorUnit, itemValor: Real;
  itemEstoque, valorEstoque: Integer;
begin
  // SE FOR A TECLA * FA�A
  If Key = #42 then
  begin
    // INDICA A QUANTIDADE PARA 1 DEFININDO-O COMO DEFAULT A FIM DE EVITAR ERROS FUTUROS
    itemQuantTemp := '1';
    // PERGUNTA A QUANTIDADE DESEJADA
    If InputQuery('CAIXA ABERTO','QUANTIDADE:',itemQuantTemp) = True Then
    begin
      itemQTD := StrToFloat(itemQuantTemp);
      Key := #13; // PRESSIONA O ENTER AUTOMAT.
    end;
  end;

  // SE FOR A TECLA ENTER
  If Key = #13 then
  begin
    // PEGA O CODIGO DE BARRA
    itemCodigo := edtCaixaCodBarra.Text;
    bancoDados.adoQProdutos.Open;

    // PROCURA O CODIGO NA QUERY
    If (bancoDados.adoQProdutos.Locate('cdbarra',itemCodigo,[loPartialKey,loCaseInsensitive])) then
    begin
      bancoDados.adoQProdutos.Locate('cdbarra',itemCodigo,[loPartialKey,loCaseInsensitive]);

      itemCodigo := bancoDados.adoQProdutos.FieldByName('cdbarra').Value;
      itemNome := bancoDados.adoQProdutos.FieldByName('nome').Value;
      itemTipo := bancoDados.adoQProdutos.FieldByName('tipo_unid').Value;
      itemValorUnit := bancoDados.adoQProdutos.FieldByName('preco').Value;
      itemEstoque := bancoDados.adoQProdutos.FieldByName('quant_estoque').Value;

      // SE A QUANTIDADE DO PRODUTO ATUAL FOR 0 INDICA 1
      if itemQTD = 0 then
        itemQTD := 1;

      // CALCULA A QUANTIDADE TOTAL DE ITENS
      itemQTDTotal := itemQTDTotal + StrToInt(FloatToStr(itemQTD));

      // SE O TIPO DO PRODUTO FOR POR PESO
      If itemTipo = 'PESO' then
      begin
        // PERGUNTA O KILOGRAMA DO PRODUTO
        If InputQuery('CAIXA ABERTO - PRODUTO PESO','DIGITE O PESO(KG):',itemQuantTemp) = True Then
        begin
          itemQTD := StrToFloat(itemQuantTemp);
          Key := #13;
        end;
      end else begin
        // ABRE A QUERY PRODUTO ATUAL PARA O MODO DE EDI��O
        bancoDados.adoQProdutos.Edit;
        // DIMINUE A QUANTIDADE DE PRODUTOS COMPRADOS
        valorEstoque := itemEstoque - StrToInt(FloatToStr(itemQTD));
        bancoDados.adoQProdutos.FieldByName('quant_estoque').Value := valorEstoque;
        bancoDados.adoQProdutos.Post;
        bancoDados.adoQProdutos.Close;
        bancoDados.adoQProdutos.Open;
      end;

      // INCREMENTA O ITEM DA LISTA
      itemNum := itemNum + 1;

      // CALCULAR O VALOR DO PRODUTO
      itemValor := itemQTD * itemValorUnit;

      // CALCULA O VALOR TOTAL DO PRODUTO
      valorTotal := valorTotal + itemValor;

      // IMPRIME NA MEMO E NO FORM O ITEM COMPRADO
      edtCaixaImpressao.Lines.Add(IntToStr(itemNum) + ' | ' + itemCodigo + ' | ' + itemNome + ' | ' + FloatToStrF(itemValorUnit,ffCurrency,18,2) + ' | x' + FloatToStr(itemQTD) + ' | ' + FloatToStrF(itemValor,ffCurrency,18,2));
      lblNomeProduto.Caption := FloatToStr(itemQTD) + 'x  ' + itemNome;
      lblValorUnit.Caption := FloatToStrF(itemValorUnit,ffCurrency,18,2);
      lblValor.Caption := FloatToStrF(itemValor,ffCurrency,18,2);
      lblvalorTotal.Caption := FloatToStrF(valorTotal,ffCurrency,18,2);

      // INSERE NA QUERY COMPRA O ITEM COMPRADO
      bancoDados.adoQCompra.Open;
      bancoDados.adoQCompra.Insert;
        bancoDados.adoQCompra.FieldByName('notafiscal').Value := notaFiscal;
        bancoDados.adoQCompra.FieldByName('item_num').Value := itemNum;
        bancoDados.adoQCompra.FieldByName('item_codigo').Value := itemCodigo;
        bancoDados.adoQCompra.FieldByName('item_nome').Value := itemNome;
        bancoDados.adoQCompra.FieldByName('item_valorunit').Value := itemValorUnit;
        bancoDados.adoQCompra.FieldByName('item_quant').Value := itemQTD;
        bancoDados.adoQCompra.FieldByName('item_valor').Value := itemValor;
      bancoDados.adoQCompra.Post;
      bancoDados.adoQCompra.Close;

      itemQTD := 0;
      edtCaixaCodBarra.Clear;
      edtCaixaCodBarra.SetFocus;
    end else begin
      MessageDlg('PRODUTO N�O ENCONTRADO!',mtInformation,[mbOk],0);
      edtCaixaCodBarra.Clear;
      edtCaixaCodBarra.SetFocus;
    end;
  end;
  
  If not (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.btnFornecedoresBuscaCnpjClick(Sender: TObject);
Var
  buscaString: String;
begin
  buscaString := ' ';
  If InputQuery('PESQUISADOR - FORNECEDOR','POR CNPJ:',buscaString) = True Then
  begin
    bancoDados.adoQFornecedores.Open;
    If (bancoDados.adoQFornecedores.Locate('cnpj',buscaString,[loPartialKey,loCaseInsensitive])) then
    begin
      bancoDados.adoQFornecedores.Locate('cnpj',buscaString,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQFornecedores.Edit;

      btnFornecedoresBuscaCnpj.Enabled := False;
      btnFornecedoresBuscaCpf.Enabled := False;

      btnFornecedoresCadastrar.Enabled := False;
      btnFornecedoresModificar.Enabled := True;
      btnFornecedoresRemover.Enabled := True;
      btnFornecedoresCancelar.Enabled := True;

      edtFornecedoresCnpj.Text := buscaString;

      edtFornecedoresCnpj.Enabled := True;
      edtFornecedoresCpf.Enabled := True;
      edtFornecedoresNome.Enabled := True;
      edtFornecedoresRazaoSocial.Enabled := True;
      edtFornecedoresLogradouro.Enabled := True;
      edtFornecedoresNum.Enabled := True;
      edtFornecedoresBairro.Enabled := True;
      edtFornecedoresCidade.Enabled := True;
      edtFornecedoresUf.Enabled := True;
      edtFornecedoresCep.Enabled := True;
      edtFornecedoresTel.Enabled := True;
      edtFornecedoresFax.Enabled := True;
      edtFornecedoresCel.Enabled := True;
      edtFornecedoresEmail.Enabled := True;

      edtFornecedoresCpf.SelectAll;
    end else begin
      if MessageDlg('N�O ENCONTRADO!' + #13 + 'DESEJA EFETUAR UM CADASTRO?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        bancoDados.adoQFornecedores.Insert;

        btnFornecedoresBuscaCnpj.Enabled := False;
        btnFornecedoresBuscaCpf.Enabled := False;

        btnFornecedoresCadastrar.Enabled := True;
        btnFornecedoresModificar.Enabled := False;
        btnFornecedoresRemover.Enabled := False;
        btnFornecedoresCancelar.Enabled := True;

        edtFornecedoresCnpj.Enabled := True;
        edtFornecedoresCpf.Enabled := True;
        edtFornecedoresNome.Enabled := True;
        edtFornecedoresRazaoSocial.Enabled := True;
        edtFornecedoresLogradouro.Enabled := True;
        edtFornecedoresNum.Enabled := True;
        edtFornecedoresBairro.Enabled := True;
        edtFornecedoresCidade.Enabled := True;
        edtFornecedoresUf.Enabled := True;
        edtFornecedoresCep.Enabled := True;
        edtFornecedoresTel.Enabled := True;
        edtFornecedoresFax.Enabled := True;
        edtFornecedoresCel.Enabled := True;
        edtFornecedoresEmail.Enabled := True;

        edtFornecedoresCnpj.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmGerente.btnFornecedoresBuscaCpfClick(Sender: TObject);
Var
  buscaString: String;
begin
  buscaString := ' ';
  If InputQuery('PESQUISADOR - FORNECEDOR','POR CPF:',buscaString) = True Then
  begin
    bancoDados.adoQFornecedores.Open;
    If (bancoDados.adoQFornecedores.Locate('cpf',buscaString,[loPartialKey,loCaseInsensitive])) then
    begin
      bancoDados.adoQFornecedores.Locate('cpf',buscaString,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQFornecedores.Edit;

      btnFornecedoresBuscaCnpj.Enabled := False;
      btnFornecedoresBuscaCpf.Enabled := False;

      btnFornecedoresCadastrar.Enabled := False;
      btnFornecedoresModificar.Enabled := True;
      btnFornecedoresRemover.Enabled := True;
      btnFornecedoresCancelar.Enabled := True;

      edtFornecedoresCpf.Text := buscaString;

      edtFornecedoresCnpj.Enabled := True;
      edtFornecedoresCpf.Enabled := True;
      edtFornecedoresNome.Enabled := True;
      edtFornecedoresRazaoSocial.Enabled := True;
      edtFornecedoresLogradouro.Enabled := True;
      edtFornecedoresNum.Enabled := True;
      edtFornecedoresBairro.Enabled := True;
      edtFornecedoresCidade.Enabled := True;
      edtFornecedoresUf.Enabled := True;
      edtFornecedoresCep.Enabled := True;
      edtFornecedoresTel.Enabled := True;
      edtFornecedoresFax.Enabled := True;
      edtFornecedoresCel.Enabled := True;
      edtFornecedoresEmail.Enabled := True;

      edtFornecedoresCpf.SelectAll;
    end else begin
      if MessageDlg('N�O ENCONTRADO!' + #13 + 'DESEJA EFETUAR UM CADASTRO?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        bancoDados.adoQFornecedores.Insert;

        btnFornecedoresBuscaCnpj.Enabled := False;
        btnFornecedoresBuscaCpf.Enabled := False;

        btnFornecedoresCadastrar.Enabled := True;
        btnFornecedoresModificar.Enabled := False;
        btnFornecedoresRemover.Enabled := False;
        btnFornecedoresCancelar.Enabled := True;

        edtFornecedoresCnpj.Enabled := True;
        edtFornecedoresCpf.Enabled := True;
        edtFornecedoresNome.Enabled := True;
        edtFornecedoresRazaoSocial.Enabled := True;
        edtFornecedoresLogradouro.Enabled := True;
        edtFornecedoresNum.Enabled := True;
        edtFornecedoresBairro.Enabled := True;
        edtFornecedoresCidade.Enabled := True;
        edtFornecedoresUf.Enabled := True;
        edtFornecedoresCep.Enabled := True;
        edtFornecedoresTel.Enabled := True;
        edtFornecedoresFax.Enabled := True;
        edtFornecedoresCel.Enabled := True;
        edtFornecedoresEmail.Enabled := True;

        edtFornecedoresCnpj.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmGerente.btnFornecedoresCadastrarClick(Sender: TObject);
begin
  bancoDados.adoQFornecedores.Post;
  bancoDados.adoQFornecedores.Close;
  bancoDados.adoQFornecedores.Open;
  bancoDados.adoQFornecedores.First;

  btnFornecedoresBuscaCnpj.Enabled := True;
  btnFornecedoresBuscaCpf.Enabled := True;

  btnFornecedoresCadastrar.Enabled := False;
  btnFornecedoresModificar.Enabled := False;
  btnFornecedoresRemover.Enabled := False;
  btnFornecedoresCancelar.Enabled := False;

  edtFornecedoresCnpj.Enabled := False;
  edtFornecedoresCpf.Enabled := False;
  edtFornecedoresNome.Enabled := False;
  edtFornecedoresRazaoSocial.Enabled := False;
  edtFornecedoresLogradouro.Enabled := False;
  edtFornecedoresNum.Enabled := False;
  edtFornecedoresBairro.Enabled := False;
  edtFornecedoresCidade.Enabled := False;
  edtFornecedoresUf.Enabled := False;
  edtFornecedoresCep.Enabled := False;
  edtFornecedoresTel.Enabled := False;
  edtFornecedoresFax.Enabled := False;
  edtFornecedoresCel.Enabled := False;
  edtFornecedoresEmail.Enabled := False;
end;

procedure TfrmGerente.btnFornecedoresModificarClick(Sender: TObject);
begin
  bancoDados.adoQFornecedores.Post;
  bancoDados.adoQFornecedores.Close;
  bancoDados.adoQFornecedores.Open;
  bancoDados.adoQFornecedores.First;

  btnFornecedoresBuscaCnpj.Enabled := True;
  btnFornecedoresBuscaCpf.Enabled := True;

  btnFornecedoresCadastrar.Enabled := False;
  btnFornecedoresModificar.Enabled := False;
  btnFornecedoresRemover.Enabled := False;
  btnFornecedoresCancelar.Enabled := False;

  edtFornecedoresCnpj.Enabled := False;
  edtFornecedoresCpf.Enabled := False;
  edtFornecedoresNome.Enabled := False;
  edtFornecedoresRazaoSocial.Enabled := False;
  edtFornecedoresLogradouro.Enabled := False;
  edtFornecedoresNum.Enabled := False;
  edtFornecedoresBairro.Enabled := False;
  edtFornecedoresCidade.Enabled := False;
  edtFornecedoresUf.Enabled := False;
  edtFornecedoresCep.Enabled := False;
  edtFornecedoresTel.Enabled := False;
  edtFornecedoresFax.Enabled := False;
  edtFornecedoresCel.Enabled := False;
  edtFornecedoresEmail.Enabled := False;
end;

procedure TfrmGerente.btnFornecedoresRemoverClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja remover: ' +  edtFornecedoresNome.Text + '?',mtWarning,[mbYes,mbNo],0) = mrYes then
  begin
    bancoDados.adoQFornecedores.Delete;
    bancoDados.adoQFornecedores.Close;
    bancoDados.adoQFornecedores.Open;
    bancoDados.adoQFornecedores.First;

    btnFornecedoresBuscaCnpj.Enabled := True;
    btnFornecedoresBuscaCpf.Enabled := True;

    btnFornecedoresCadastrar.Enabled := False;
    btnFornecedoresModificar.Enabled := False;
    btnFornecedoresRemover.Enabled := False;
    btnFornecedoresCancelar.Enabled := False;

    edtFornecedoresCnpj.Enabled := False;
    edtFornecedoresCpf.Enabled := False;
    edtFornecedoresNome.Enabled := False;
    edtFornecedoresRazaoSocial.Enabled := False;
    edtFornecedoresLogradouro.Enabled := False;
    edtFornecedoresNum.Enabled := False;
    edtFornecedoresBairro.Enabled := False;
    edtFornecedoresCidade.Enabled := False;
    edtFornecedoresUf.Enabled := False;
    edtFornecedoresCep.Enabled := False;
    edtFornecedoresTel.Enabled := False;
    edtFornecedoresFax.Enabled := False;
    edtFornecedoresCel.Enabled := False;
    edtFornecedoresEmail.Enabled := False;
  end;
end;

procedure TfrmGerente.btnFornecedoresCancelarClick(Sender: TObject);
begin
  bancoDados.adoQFornecedores.Cancel;
  bancoDados.adoQFornecedores.Close;
  bancoDados.adoQFornecedores.Open;
  bancoDados.adoQFornecedores.First;

  btnFornecedoresBuscaCnpj.Enabled := True;
  btnFornecedoresBuscaCpf.Enabled := True;

  btnFornecedoresCadastrar.Enabled := False;
  btnFornecedoresModificar.Enabled := False;
  btnFornecedoresRemover.Enabled := False;
  btnFornecedoresCancelar.Enabled := False;

  edtFornecedoresCnpj.Enabled := False;
  edtFornecedoresCpf.Enabled := False;
  edtFornecedoresNome.Enabled := False;
  edtFornecedoresRazaoSocial.Enabled := False;
  edtFornecedoresLogradouro.Enabled := False;
  edtFornecedoresNum.Enabled := False;
  edtFornecedoresBairro.Enabled := False;
  edtFornecedoresCidade.Enabled := False;
  edtFornecedoresUf.Enabled := False;
  edtFornecedoresCep.Enabled := False;
  edtFornecedoresTel.Enabled := False;
  edtFornecedoresFax.Enabled := False;
  edtFornecedoresCel.Enabled := False;
  edtFornecedoresEmail.Enabled := False;
end;

procedure TfrmGerente.edtFornecedorescpfKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresCnpjKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresTelKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresFaxKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresCelKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresCEPKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresNumKeyPress(Sender: TObject; var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;


procedure TfrmGerente.edtFornecedoresNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresRazaoSocialKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresLogradouroKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresBairroKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresCidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresUfKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtFornecedoresEmailKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;












procedure TfrmGerente.cbxProdutosFornecedoresChange(Sender: TObject);
begin
  cbxProdutosFornecedoresHide.ItemIndex := cbxProdutosFornecedores.ItemIndex;
end;

procedure TfrmGerente.btnProdutosBuscaCodBarrasClick(Sender: TObject);
Var
  buscaString, codFornecedor, nomeFornecedor: String;
begin
  buscaString := ' ';
  cbxProdutosFornecedores.Clear;
  cbxProdutosFornecedoresHide.Clear;
  bancoDados.adoQFornecedores.Open;
  bancoDados.adoQFornecedores.First;
  while (not bancoDados.adoQFornecedores.Eof) do
  begin
    codFornecedor := bancoDados.adoQFornecedores.FieldByName('codigo').Value;
    nomeFornecedor := bancoDados.adoQFornecedores.FieldByName('nome').Value;
    cbxProdutosFornecedores.Items.Add(codFornecedor + ' - ' + nomeFornecedor);
    cbxProdutosFornecedoresHide.Items.Add(codFornecedor);
    bancoDados.adoQFornecedores.Next;
  end;
  bancoDados.adoQFornecedores.Close;
  If InputQuery('PESQUISADOR - PRODUTO','POR C�DIGO DE BARRA:',buscaString) = True Then
  begin
    bancoDados.adoQProdutos.Open;
    If (bancoDados.adoQProdutos.Locate('cdbarra',buscaString,[loPartialKey,loCaseInsensitive])) then
    begin
      bancoDados.adoQProdutos.Locate('cdbarra',buscaString,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQProdutos.Edit;

      btnProdutosBuscaCodBarras.Enabled := False;
      btnProdutosBuscaNome.Enabled := False;

      btnProdutosCadastrar.Enabled := False;
      btnProdutosModificar.Enabled := True;
      btnProdutosRemover.Enabled := True;
      btnProdutosCancelar.Enabled := True;

      edtProdutosCodBarras.Text := buscaString;

      edtProdutosCodBarras.Enabled := True;
      edtProdutosNome.Enabled := True;
      edtProdutosPreco.Enabled := True;
      edtProdutosTipo.Enabled := True;
      edtProdutosEstoque.Enabled := True;
      edtProdutosDescricao.Enabled := True;
      cbxProdutosFornecedores.Enabled := True;
      cbxProdutosFornecedoresHide.Enabled := True;

      bancoDados.adoQFornecedores.Open;
      bancoDados.adoQFornecedores.Locate('codigo',bancoDados.adoQProdutos.FieldByName('cod_fornecedor').Value,[loPartialKey,loCaseInsensitive]);
      cbxProdutosFornecedores.ItemIndex := cbxProdutosFornecedores.Items.IndexOf(IntToStr(bancoDados.adoQFornecedores.FieldByName('codigo').Value) + ' - ' + bancoDados.adoQFornecedores.FieldByName('nome').Value);
      cbxProdutosFornecedoresHide.ItemIndex := cbxProdutosFornecedores.ItemIndex;

      edtProdutosCodBarras.SetFocus;
    end else begin
      if MessageDlg('N�O ENCONTRADO!' + #13 + 'DESEJA EFETUAR UM CADASTRO?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        bancoDados.adoQProdutos.Insert;

        btnProdutosBuscaCodBarras.Enabled := False;
        btnProdutosBuscaNome.Enabled := False;

        btnProdutosCadastrar.Enabled := True;
        btnProdutosModificar.Enabled := False;
        btnProdutosRemover.Enabled := False;
        btnProdutosCancelar.Enabled := True;

        edtProdutosCodBarras.Enabled := True;
        edtProdutosNome.Enabled := True;
        edtProdutosPreco.Enabled := True;
        edtProdutosTipo.Enabled := True;
        edtProdutosEstoque.Enabled := True;
        edtProdutosDescricao.Enabled := True;
        cbxProdutosFornecedores.Enabled := True;
        cbxProdutosFornecedoresHide.Enabled := True;

        edtProdutosCodBarras.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmGerente.btnProdutosBuscaNomeClick(Sender: TObject);
Var
  buscaString, codFornecedor, nomeFornecedor: String;
begin
  buscaString := ' ';
  cbxProdutosFornecedores.Clear;
  cbxProdutosFornecedoresHide.Clear;
  bancoDados.adoQFornecedores.Open;
  bancoDados.adoQFornecedores.First;
  while (not bancoDados.adoQFornecedores.Eof) do
  begin
    codFornecedor := bancoDados.adoQFornecedores.FieldByName('codigo').Value;
    nomeFornecedor := bancoDados.adoQFornecedores.FieldByName('nome').Value;
    cbxProdutosFornecedores.Items.Add(codFornecedor + ' - ' + nomeFornecedor);
    cbxProdutosFornecedoresHide.Items.Add(codFornecedor);
    bancoDados.adoQFornecedores.Next;
  end;
  bancoDados.adoQFornecedores.Close;
  If InputQuery('PESQUISADOR - PRODUTO','POR NOME:',buscaString) = True Then
  begin
    bancoDados.adoQProdutos.Open;
    If (bancoDados.adoQProdutos.Locate('nome',buscaString,[loPartialKey,loCaseInsensitive])) then
    begin
      bancoDados.adoQProdutos.Locate('nome',buscaString,[loPartialKey,loCaseInsensitive]);
      bancoDados.adoQProdutos.Edit;

      btnProdutosBuscaCodBarras.Enabled := False;
      btnProdutosBuscaNome.Enabled := False;

      btnProdutosCadastrar.Enabled := False;
      btnProdutosModificar.Enabled := True;
      btnProdutosRemover.Enabled := True;
      btnProdutosCancelar.Enabled := True;

      edtProdutosNome.Text := buscaString;

      edtProdutosCodBarras.Enabled := True;
      edtProdutosNome.Enabled := True;
      edtProdutosPreco.Enabled := True;
      edtProdutosTipo.Enabled := True;
      edtProdutosEstoque.Enabled := True;
      edtProdutosDescricao.Enabled := True;
      cbxProdutosFornecedores.Enabled := True;
      cbxProdutosFornecedoresHide.Enabled := True;

      bancoDados.adoQFornecedores.Open;
      bancoDados.adoQFornecedores.Locate('codigo',bancoDados.adoQProdutos.FieldByName('cod_fornecedor').Value,[loPartialKey,loCaseInsensitive]);
      cbxProdutosFornecedores.ItemIndex := cbxProdutosFornecedores.Items.IndexOf(IntToStr(bancoDados.adoQFornecedores.FieldByName('codigo').Value) + ' - ' + bancoDados.adoQFornecedores.FieldByName('nome').Value);
      cbxProdutosFornecedoresHide.ItemIndex := cbxProdutosFornecedores.ItemIndex;

      edtProdutosCodBarras.SetFocus;
    end else begin
      if MessageDlg('N�O ENCONTRADO!' + #13 + 'DESEJA EFETUAR UM CADASTRO?',mtWarning,[mbYes,mbNo],0) = mrYes then
      begin
        bancoDados.adoQProdutos.Insert;

        btnProdutosBuscaCodBarras.Enabled := False;
        btnProdutosBuscaNome.Enabled := False;

        btnProdutosCadastrar.Enabled := True;
        btnProdutosModificar.Enabled := False;
        btnProdutosRemover.Enabled := False;
        btnProdutosCancelar.Enabled := True;

        edtProdutosCodBarras.Enabled := True;
        edtProdutosNome.Enabled := True;
        edtProdutosPreco.Enabled := True;
        edtProdutosTipo.Enabled := True;
        edtProdutosEstoque.Enabled := True;
        edtProdutosDescricao.Enabled := True;
        cbxProdutosFornecedores.Enabled := True;
        cbxProdutosFornecedoresHide.Enabled := True;

        edtProdutosCodBarras.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmGerente.btnProdutosCadastrarClick(Sender: TObject);
begin
  bancoDados.adoQProdutos.FieldByName('cod_fornecedor').Value := StrToInt(cbxProdutosFornecedoresHide.Text);
  bancoDados.adoQProdutos.Post;
  bancoDados.adoQProdutos.Close;
  bancoDados.adoQProdutos.Open;
  bancoDados.adoQProdutos.First;

  btnProdutosBuscaCodBarras.Enabled := True;
  btnProdutosBuscaNome.Enabled := True;

  btnProdutosCadastrar.Enabled := False;
  btnProdutosModificar.Enabled := False;
  btnProdutosRemover.Enabled := False;
  btnProdutosCancelar.Enabled := False;

  edtProdutosCodBarras.Enabled := False;
  edtProdutosNome.Enabled := False;
  edtProdutosPreco.Enabled := False;
  edtProdutosTipo.Enabled := False;
  edtProdutosEstoque.Enabled := False;
  edtProdutosDescricao.Enabled := False;
  cbxProdutosFornecedores.Enabled := False;
  cbxProdutosFornecedoresHide.Enabled := False;
end;

procedure TfrmGerente.btnProdutosModificarClick(Sender: TObject);
begin
  bancoDados.adoQProdutos.FieldByName('cod_fornecedor').Value := StrToInt(cbxProdutosFornecedoresHide.Text);
  bancoDados.adoQProdutos.Post;
  bancoDados.adoQProdutos.Close;
  bancoDados.adoQProdutos.Open;
  bancoDados.adoQProdutos.First;

  btnProdutosBuscaCodBarras.Enabled := True;
  btnProdutosBuscaNome.Enabled := True;

  btnProdutosCadastrar.Enabled := False;
  btnProdutosModificar.Enabled := False;
  btnProdutosRemover.Enabled := False;
  btnProdutosCancelar.Enabled := False;

  edtProdutosCodBarras.Enabled := False;
  edtProdutosNome.Enabled := False;
  edtProdutosPreco.Enabled := False;
  edtProdutosTipo.Enabled := False;
  edtProdutosEstoque.Enabled := False;
  edtProdutosDescricao.Enabled := False;
  cbxProdutosFornecedores.Enabled := False;
  cbxProdutosFornecedoresHide.Enabled := False;
end;

procedure TfrmGerente.btnProdutosRemoverClick(Sender: TObject);
begin
  if MessageDlg('Voc� deseja remover: ' +  edtProdutosNome.Text + '?',mtWarning,[mbYes,mbNo],0) = mrYes then
  begin
    bancoDados.adoQProdutos.Delete;
    bancoDados.adoQProdutos.Close;
    bancoDados.adoQProdutos.Open;
    bancoDados.adoQProdutos.First;

    btnProdutosBuscaCodBarras.Enabled := True;
    btnProdutosBuscaNome.Enabled := True;

    btnProdutosCadastrar.Enabled := False;
    btnProdutosModificar.Enabled := False;
    btnProdutosRemover.Enabled := False;
    btnProdutosCancelar.Enabled := False;

    edtProdutosCodBarras.Enabled := False;
    edtProdutosNome.Enabled := False;
    edtProdutosPreco.Enabled := False;
    edtProdutosTipo.Enabled := False;
    edtProdutosEstoque.Enabled := False;
    edtProdutosDescricao.Enabled := False;
    cbxProdutosFornecedores.Enabled := False;
    cbxProdutosFornecedoresHide.Enabled := False;
  end;
end;

procedure TfrmGerente.btnProdutosCancelarClick(Sender: TObject);
begin
  bancoDados.adoQProdutos.Cancel;
  bancoDados.adoQProdutos.Close;
  bancoDados.adoQProdutos.Open;
  bancoDados.adoQProdutos.First;

  btnProdutosBuscaCodBarras.Enabled := True;
  btnProdutosBuscaNome.Enabled := True;

  btnProdutosCadastrar.Enabled := False;
  btnProdutosModificar.Enabled := False;
  btnProdutosRemover.Enabled := False;
  btnProdutosCancelar.Enabled := False;

  edtProdutosCodBarras.Enabled := False;
  edtProdutosNome.Enabled := False;
  edtProdutosPreco.Enabled := False;
  edtProdutosTipo.Enabled := False;
  edtProdutosEstoque.Enabled := False;
  edtProdutosDescricao.Enabled := False;
  cbxProdutosFornecedores.Enabled := False;
  cbxProdutosFornecedoresHide.Enabled := False;
end;

procedure TfrmGerente.edtClientesNumKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtProdutosCodBarrasKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtProdutosEstoqueKeyPress(Sender: TObject;
  var Key: Char);
begin
  If NOT (Key in ['0'..'9',#8]) Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtProdutosNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtProdutosDescricaoKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesNomeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesLogradouroKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesBairroKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesCidadeKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesUfKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.edtClientesDependentesKeyPress(Sender: TObject;
  var Key: Char);
begin
  If Key = #80 Then
  begin
    Key := #0;
    Beep;
  end;
end;

procedure TfrmGerente.btnFecharClick(Sender: TObject);
begin
  Close;
end;

end.
