object bancoDados: TbancoDados
  OldCreateOrder = False
  Left = 852
  Top = 110
  Height = 513
  Width = 169
  object adoConn: TADOConnection
    Connected = True
    ConnectionString = 
      'Provider=Microsoft.Jet.OLEDB.4.0;Password="";User ID=Admin;Data ' +
      'Source=.\fisCalDB.mdb;Mode=Share Deny None;Extended Properties="' +
      '";Jet OLEDB:System database="";Jet OLEDB:Registry Path="";Jet OL' +
      'EDB:Database Password=bulling2006;Jet OLEDB:Engine Type=5;Jet OL' +
      'EDB:Database Locking Mode=1;Jet OLEDB:Global Partial Bulk Ops=2;' +
      'Jet OLEDB:Global Bulk Transactions=1;Jet OLEDB:New Database Pass' +
      'word="";Jet OLEDB:Create System Database=False;Jet OLEDB:Encrypt' +
      ' Database=False;Jet OLEDB:Don'#39't Copy Locale on Compact=False;Jet' +
      ' OLEDB:Compact Without Replica Repair=False;Jet OLEDB:SFP=False'
    LoginPrompt = False
    Mode = cmShareDenyNone
    Provider = 'Microsoft.Jet.OLEDB.4.0'
    Left = 48
    Top = 20
  end
  object adoQConfiguracao: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM configuracao;')
    Left = 48
    Top = 68
    object adoQConfiguracaologin: TWideStringField
      FieldName = 'login'
      Size = 255
    end
    object adoQConfiguracaosenha: TWideStringField
      FieldName = 'senha'
      Size = 255
    end
    object adoQConfiguracaonome: TWideStringField
      FieldName = 'nome'
      Size = 255
    end
    object adoQConfiguracaorazaosocial: TWideStringField
      FieldName = 'razaosocial'
      Size = 255
    end
    object adoQConfiguracaocnpj: TWideStringField
      FieldName = 'cnpj'
      Size = 15
    end
    object adoQConfiguracaocpf: TWideStringField
      FieldName = 'cpf'
      Size = 12
    end
    object adoQConfiguracaoie: TWideStringField
      FieldName = 'ie'
      Size = 15
    end
  end
  object adoQFornecedores: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM fornecedores;')
    Left = 48
    Top = 116
    object adoQFornecedorescodigo: TAutoIncField
      FieldName = 'codigo'
      ReadOnly = True
    end
    object adoQFornecedoresnome: TWideStringField
      FieldName = 'nome'
      Size = 255
    end
    object adoQFornecedoresrazaosocial: TWideStringField
      FieldName = 'razaosocial'
      Size = 255
    end
    object adoQFornecedorescnpj: TWideStringField
      FieldName = 'cnpj'
      Size = 14
    end
    object adoQFornecedorescpf: TWideStringField
      FieldName = 'cpf'
      Size = 12
    end
    object adoQFornecedoresend_logradouro: TWideStringField
      FieldName = 'end_logradouro'
      Size = 255
    end
    object adoQFornecedoresend_bairro: TWideStringField
      FieldName = 'end_bairro'
      Size = 255
    end
    object adoQFornecedoresend_num: TIntegerField
      FieldName = 'end_num'
    end
    object adoQFornecedoresend_cep: TWideStringField
      FieldName = 'end_cep'
      Size = 8
    end
    object adoQFornecedoresend_cidade: TWideStringField
      FieldName = 'end_cidade'
      Size = 255
    end
    object adoQFornecedoresend_uf: TWideStringField
      FieldName = 'end_uf'
      Size = 255
    end
    object adoQFornecedorestelefone: TWideStringField
      FieldName = 'telefone'
      Size = 10
    end
    object adoQFornecedoresfax: TWideStringField
      FieldName = 'fax'
      Size = 10
    end
    object adoQFornecedorescelular: TWideStringField
      FieldName = 'celular'
      Size = 10
    end
    object adoQFornecedoresemail: TWideStringField
      FieldName = 'email'
      Size = 255
    end
  end
  object adoQProdutos: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM produtos;')
    Left = 48
    Top = 168
    object adoQProdutoscodigo: TAutoIncField
      FieldName = 'codigo'
      ReadOnly = True
    end
    object adoQProdutoscdbarra: TWideStringField
      FieldName = 'cdbarra'
      Size = 255
    end
    object adoQProdutosnome: TWideStringField
      FieldName = 'nome'
      Size = 255
    end
    object adoQProdutospreco: TBCDField
      FieldName = 'preco'
      currency = True
      Precision = 19
    end
    object adoQProdutostipo_unid: TWideStringField
      FieldName = 'tipo_unid'
      Size = 50
    end
    object adoQProdutosdescricao: TMemoField
      FieldName = 'descricao'
      BlobType = ftMemo
    end
    object adoQProdutoscod_fornecedor: TIntegerField
      FieldName = 'cod_fornecedor'
    end
    object adoQProdutosquant_estoque: TIntegerField
      FieldName = 'quant_estoque'
    end
  end
  object adoQNotaFiscal: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM notafiscal;')
    Left = 48
    Top = 216
    object adoQNotaFiscalcodigo: TAutoIncField
      FieldName = 'codigo'
      ReadOnly = True
    end
    object adoQNotaFiscalquant_produto: TIntegerField
      FieldName = 'quant_produto'
    end
    object adoQNotaFiscalvalor_total: TBCDField
      FieldName = 'valor_total'
      currency = True
      Precision = 19
    end
    object adoQNotaFiscalvalor_pago: TBCDField
      FieldName = 'valor_pago'
      currency = True
      Precision = 19
    end
    object adoQNotaFiscalvalor_troco: TBCDField
      FieldName = 'valor_troco'
      currency = True
      Precision = 19
    end
    object adoQNotaFiscalstatus: TWideStringField
      FieldName = 'status'
      Size = 50
    end
  end
  object adoQCompra: TADOQuery
    Active = True
    Connection = adoConn
    CursorType = ctStatic
    Parameters = <>
    SQL.Strings = (
      'SELECT * FROM compra;')
    Left = 48
    Top = 264
    object adoQCompracodigo: TAutoIncField
      FieldName = 'codigo'
      ReadOnly = True
    end
    object adoQCompranotafiscal: TIntegerField
      FieldName = 'notafiscal'
    end
    object adoQCompraitem_num: TIntegerField
      FieldName = 'item_num'
    end
    object adoQCompraitem_codigo: TWideStringField
      FieldName = 'item_codigo'
      Size = 255
    end
    object adoQCompraitem_nome: TWideStringField
      FieldName = 'item_nome'
      Size = 255
    end
    object adoQCompraitem_valorunit: TBCDField
      FieldName = 'item_valorunit'
      currency = True
      Precision = 19
    end
    object adoQCompraitem_quant: TBCDField
      FieldName = 'item_quant'
      Precision = 18
      Size = 0
    end
    object adoQCompraitem_valor: TBCDField
      FieldName = 'item_valor'
      currency = True
      Precision = 19
    end
  end
end
